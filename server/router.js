const url = require('url');

class Router {
    #routes = new Map();

    handleRequest = (req, res) => {
        const parsedUrl =  url.parse(req.url);
        const methods = this.#routes.get(parsedUrl.pathname);
        if(!(methods && methods.hasOwnProperty(req.method))) {
            return res.end();
        }
        methods[req.method](req, res);
    }

    get(url, callback) {
        this.addRoute('GET', url, callback);
    }

    post(url, callback) {
        this.addRoute('POST', url, callback);
    }

    addRoute(method, url, callback) {
        if(!this.#routes.has(url)) {
            this.#routes.set(url, {});
        }

        const urlMethods = this.#routes.get(url);
        urlMethods[method] = callback;
    }
}

module.exports = {
    Router
};