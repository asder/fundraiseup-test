const http = require('http');
const {Router} = require('./router');

const router = new Router();

router.post('/data', (req, res) => {
    let data = '';
    req.on('data', (chunck) => {
        data += chunck;
    });
    req.on('end', () => {
        const expectation = Math.random() * 100;
        if(expectation <= 60) {
            try {
                const json = JSON.parse(data);
                console.log(json);
            } catch(e) {
                console.error(e);
                res.writeHead(400);
                res.write('Invalid JSON');
                res.end();
                return;
            }
            res.writeHead(200);
            res.write('OK');
            res.end();
        } else if(expectation > 60 && expectation <= 80) {
            res.writeHead(500);
            res.end();
        } else {
            //keeping connection
        }
    });

    req.on('error', (e) => console.error(e));
});

const server = http.createServer(router.handleRequest);

server.on('error', (e) => console.error(e));

server.listen(process.env.PORT || 8080, () => {
    console.log('server is running on port ' + server.address().port);
});
