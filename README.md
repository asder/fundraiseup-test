# fundraiseup-test

## Available scripts
```sh
npm start
```

Runs Server and Client in the same thread.

```sh
npm run client
```

Runs Client

```sh
npm run server
```

Runs Server (default port 8080)

## Environment variables

PORT changes Server port

SERVER_URL is used on Client, when Server url is different from default http://localhost:8080/data

PING_URL the Clients url to ping (default https://fundraiseup.com/)