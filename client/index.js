const {requestToServer, pingUrl} = require('./requests');
const config = require('./config');

let pingId = 0;

ping();

function ping() {
    const pingData = {
        pingId,
        deliveryAttempt: 0,
        date: Date.now()
    };
    pingId++;

    pingUrl(config.pingUrl)
        .then(({responseTime}) => {
            pingData.responseTime = responseTime;
            sendRequestToServer(pingData);
        })
        .catch((e) => console.error(e));

    setTimeout(ping, config.pingInterval);
}

function sendRequestToServer(pingData) {
    pingData.deliveryAttempt++;
    requestToServer(config.serverUrl, pingData)
        .then((result) => {
            console.log(`PingId ${pingData.pingId} deliveryAttempt ${pingData.deliveryAttempt} ${result.statusCode ? `${result.statusCode} ${result.data}` : result.error}`);
            if(result.error || result.statusCode !== 200) {
                setTimeout(() => sendRequestToServer(pingData), Math.pow(2, pingData.deliveryAttempt) * 1000);
            }
        });
}
  


