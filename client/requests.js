const http = require('http');
const https = require('https');
const url = require('url');
const {serverRequestTimeout} = require('./config');

const requestClients = {
    'http:': http,
    'https:': https
};

function requestToServer(serverUrl, json) {
    return new Promise((resolve) => {
        const parsedUrl = url.parse(serverUrl);
        const port = parsedUrl.port || parsedUrl.protocol === 'http:' && 80 || 443;

        const data = JSON.stringify(json);

        const options = {
            host: parsedUrl.hostname,
            path: parsedUrl.path,
            method: 'POST',
            port,
            headers: {
                'Content-Type': 'application/json',
                'Content-Length': Buffer.byteLength(data)
            }
        };

        let timeoutId;

        const request = requestClients[parsedUrl.protocol].request(options, (res) => {
            clearTimeout(timeoutId);

            let data = '';
            res.on('data', (chunk) => {
                data += chunk;
            });

            res.on('end', () => {
                resolve({statusCode: res.statusCode, data});
            });
        });

        request.on('error', (e) => {
            resolve({error: 'serverRequestError'});
        });

        request.write(data)
        request.end();

        timeoutId = setTimeout(() => {
            request.destroy();
            resolve({error: 'serverRequestTimeout'});
        }, serverRequestTimeout);
    });
}

function pingUrl(someUrl) {
    return new Promise((resolve, reject) => {
        const parsedUrl = url.parse(someUrl);
        const port = parsedUrl.port || parsedUrl.protocol === 'http:' && 80 || 443;
        const start = Date.now();
        const options = {
            host: parsedUrl.hostname,
            path: parsedUrl.path,
            method: 'GET',
            port
        };
        
        const request = requestClients[parsedUrl.protocol].request(options, (res) => {
            request.destroy();
            resolve({
                responseTime: Date.now() - start
            });
        });

        request.on('error', (e) => {
            reject({error: e});
        });

        request.end();
    });
}

module.exports = {
    requestToServer,
    pingUrl
};