module.exports = {
    serverUrl: process.env.SERVER_URL || 'http://localhost:8080/data',
    serverRequestTimeout: 10 * 1000,
    pingUrl: process.env.PING_URL || 'https://fundraiseup.com/',
    pingInterval: 1000
};